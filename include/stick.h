/****************************************************************************
 *Copyright 2019 Cornelius Sevald-Krause                                    *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifndef STICK_H
#define STICK_H

#include <stdbool.h>

struct stick {
        int *discs;
        int index;
        int top;
        struct stick *next;
        struct stick *prev;
};

struct stick *init_sticks (int disc_count);
void push_to_stick (struct stick *stick, int disc);
int peek_stick (struct stick *stick);
int pop_from_stick (struct stick *stick);
bool is_stick_empty (struct stick *stick);
void delete_sticks (struct stick *stick);

#endif /* STICK_H */
