/****************************************************************************
 *Copyright 2019 Cornelius Sevald-Krause                                    *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "stick.h"


struct stick *init_sticks (int disc_count) {
        struct stick *stick_a = malloc(sizeof(struct stick));
        struct stick *stick_b = malloc(sizeof(struct stick));
        struct stick *stick_c = malloc(sizeof(struct stick));

        stick_a->discs = calloc(disc_count, sizeof(int));
        stick_b->discs = calloc(disc_count, sizeof(int));
        stick_c->discs = calloc(disc_count, sizeof(int));

        stick_a->index = 0;
        stick_b->index = 1;
        stick_c->index = 2;

        stick_b->top = stick_c->top = -1;

        stick_a->next = stick_b;
        stick_b->next = stick_c;
        stick_c->next = stick_a;

        stick_a->prev = stick_c;
        stick_b->prev = stick_a;
        stick_c->prev = stick_b;

        for (int i = 0; i < disc_count; i++) {
                stick_a->discs[i] = disc_count - i - 1;
        }
        stick_a->top = disc_count - 1;

        return stick_a;
}

void push_to_stick (struct stick *stick, int disc) {
        stick->discs[++stick->top] = disc;
}

int peek_stick (struct stick *stick) {
        return stick->discs[stick->top];
}

int pop_from_stick (struct stick *stick) {
        return stick->discs[stick->top--];
}

bool is_stick_empty (struct stick *stick) {
        return stick->top == -1;
}

void delete_sticks (struct stick *stick) {
        struct stick *stick_a = stick;
        struct stick *stick_b = stick->next;
        struct stick *stick_c = stick->next->next;

        free(stick_a->discs);
        free(stick_b->discs);
        free(stick_c->discs);

        free(stick_a);
        free(stick_b);
        free(stick_c);
}

