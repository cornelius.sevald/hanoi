/****************************************************************************
 *Copyright 2019 Cornelius Sevald-Krause                                    *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stick.h"

static void solve_hanoi (int disc_count);
static void move_discs (int index);
static void print_usage (FILE *out);

const char *progname;

static int disc_count;
static int *discs;
static struct stick *sticks[3];

int main (int argc, char **argv) {
        progname = argv[0];
        /* Parse command-line options. */
        for (char **arg=argv; *arg; arg++) {
                if (!strcmp(*arg, "--help") || !strcmp(*arg, "-h")) {
                        printf("Solve the towers of hanoi.\n");
                        print_usage(stdout);
                        return 0;
                }
        } 
        if (argc == 2) {
                char *ptr;
                disc_count = (int) strtol(argv[1], &ptr, 10);
                /* Command line argument is not a valid positive integer. */
                if (*ptr || disc_count < 1) {
                        fprintf(stderr, "'%s' is not a valid number of discs.\n", argv[1]);
                        print_usage(stderr);
                        return 1;
                }
        }
        else {
                fprintf(stderr, "Invalid amount of arguments: %d.\n", argc-1);
                print_usage(stderr);
                return 1;
        }

        /* Set up sticks & discs. */
        discs = calloc(disc_count, sizeof(int));

        struct stick *stick = init_sticks(disc_count);
        sticks[0] = stick;
        sticks[1] = stick->next;
        sticks[2] = stick->next->next;

        /* Solve the towers of hanoi. */
        solve_hanoi(disc_count);

        /* Clean up allocated memory. */
        delete_sticks(stick);
        free(discs);
}

static void solve_hanoi (int disc_count) {
        int k = disc_count;
        printf("%d", k);
        while (k > 0) {
                move_discs (--k);
        }
}

static void move_discs (int index) {
        static int count = 0;

        char m1, m2;

        int k = index;
        while (k > 0) {
                move_discs (--k);
        }

        m1 = discs[index] + 'A';

        count++;
        int disc = discs[index];
        pop_from_stick(sticks[disc]);

        struct stick *to = disc_count & 1 ? sticks[disc]->prev : sticks[disc]->next;
        if (!is_stick_empty(to) && peek_stick(to) < index) {
                to = disc_count & 1 ? to->prev : to->next;
        }
        discs[index] = to->index;
        push_to_stick(to, index);

        m2 = discs[index] + 'A';
        printf("%c%c", m1, m2);
}

static void print_usage (FILE *out) {
        fprintf(out, "Usage: %s <disc count>\n", progname);
}
