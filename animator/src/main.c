/****************************************************************************
 *Copyright 2019 Cornelius Sevald-Krause                                    *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/
#ifdef _WIN32
#ifdef __MINGW32__
    #include <windows.h>
#else
    #include <Windows.h>
#endif
#else
    #include <unistd.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifdef __MINGW32__
    #include <GL/freeglut.h>
#else
    #include <GL/glut.h>
#endif

#ifdef _WIN32
#define MILLISLEEP(x) { \
    Sleep(x);           \
    }
#else
#define MILLISLEEP(x) { \
    usleep(x*1000);     \
    }
#endif

#define MIN(a, b) a < b ? a : b
#define MAX(a, b) a < b ? b : a

#define WIN_WIDTH      800
#define WIN_HEIGHT     800
#define WIN_XPOS       10
#define WIN_YPOS       10
#define MIN_DISC_WIDTH 0.1
#define MAX_DISC_WIDTH 0.5

static void update (void);
static void display (void);

int disc_count;
int *sticks[3];
int stick_disc_counts[3] = {-1, -1, -1};
double disc_height;
int sleep_time;
GLbyte *colors;

int main (int argc, char **argv) {
        scanf("%d", &disc_count);
        for (int i = 0; i < 3; i++) {
                sticks[i] = calloc(disc_count, sizeof(int));
        }
        for (int i = 0; i < disc_count; i++) {
                sticks[0][i] = disc_count - i - 1;
        }
        stick_disc_counts[0] = disc_count - 1;
        disc_height = 1.0 / disc_count;
        
        if (disc_count >= sizeof(int) * 8)
                sleep_time = 0;
        else
            sleep_time = 5000 / (1 << disc_count);

        /* Initializethe colors. */
        colors = malloc(sizeof(GLbyte) * disc_count * 3);
        if (disc_count > 1) {
                for (int i = 0; i < disc_count; i++) {
                        double t = i / (double) (disc_count - 1);

                        double r = MAX(0.0, -2*t + 1.0);
                        double g = t > 0.5 ? -2 * (t - 1 / 2.0) + 1 : 2 * t;
                        double b = MAX(0.0,  2*t - 1.0);

                        GLbyte R = (GLbyte) (r / sqrt(r*r + g*g + b*b) * 127);
                        GLbyte G = (GLbyte) (g / sqrt(r*r + g*g + b*b) * 127);
                        GLbyte B = (GLbyte) (b / sqrt(r*r + g*g + b*b) * 127);

                        colors[i*3+0] = R;
                        colors[i*3+1] = G;
                        colors[i*3+2] = B;
                }
        } else {
                colors[0] = (GLbyte) 0;
                colors[1] = (GLbyte) 127;
                colors[2] = (GLbyte) 0;
        }

        glutInit(&argc, argv);                      // Initialize GLUT
        glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT);  // Set the window's initial width & height
        glutInitWindowPosition(WIN_XPOS, WIN_YPOS); // Position the window's initial top-left corner
        glutCreateWindow("Hanoi");                  // Create a window with the given title
        glutDisplayFunc(display);
        glutIdleFunc(update);

        glutMainLoop();
}

static void update (void) {
        char m1 = getc(stdin);
        char m2 = getc(stdin);

        if (m1 != 'A' && m1 != 'B' && m1 != 'C')
                return;
        if (m2 != 'A' && m2 != 'B' && m2 != 'C')
                return;
        m1 -= 'A';
        m2 -= 'A';

        sticks[m2][++stick_disc_counts[m2]] = sticks[m1][stick_disc_counts[m1]--];
        
        display();

        MILLISLEEP(sleep_time);
}

static void display (void) {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
        glClear(GL_COLOR_BUFFER_BIT);         // Clear the color buffer

        // Draw a Red 1x1 Square centered at origin
        glBegin(GL_QUADS);

        for (int i = 0; i < 3; i++) {
                for (int j = 0; j <= stick_disc_counts[i]; j++) {
                        int disc = sticks[i][j];
                        double x = -1/2.0 + i/2.0;
                        double y = disc_height / 2.0 + disc_height * j - 1;
                        double disc_width;
                        if (disc_count > 1) {
                                disc_width = MIN_DISC_WIDTH + (MAX_DISC_WIDTH - MIN_DISC_WIDTH) * disc / (disc_count - 1);
                        } else {
                                disc_width = (MAX_DISC_WIDTH + MIN_DISC_WIDTH) / 2.0;
                        }

                        glColor3b(colors[disc*3+0], colors[disc*3+1], colors[disc*3+2]);
                        glVertex2d(x - disc_width / 2.0, y - disc_height / 2.0);
                        glVertex2d(x + disc_width / 2.0, y - disc_height / 2.0);
                        glVertex2d(x + disc_width / 2.0, y + disc_height / 2.0);
                        glVertex2d(x - disc_width / 2.0, y + disc_height / 2.0);
                }
        }
        glEnd();

        glFlush();  // Render now
}

void deinit_graphics () {
}
