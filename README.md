Hanoi
=====

A simple hanoi solver & displayer.

Made as a school project.

Building
---------

To build, use CMake:

```bash
 $ mkdir build
 $ cd build/
 $ cmake ..
```

Usage
-----

To use, run the `solver` program with the amount of discs as a command-line argument,
and pipe the result to the animator.

```bash
 $ ./solver 10 | ./animator
```
